
# coding: utf-8

# # Fresnel and Fraunhofer propagators
# (Based on *Computational Fourier Optics: A MATLAB Tutorial* by David Voelz)
# 
# For $\Delta x$ sampling step size, $\lambda$ wavelength, $z$ propagation distance, $L$ the length of the smallest side of the support, $w$ largest feature size.
# 
# If Fresnel Number $N_F = w^2 / \lambda z << 1$ (say $N_F < 0.1$), then use Fraunhofer propagation, otherwise use Fresnel propagation.
# 
# Choice of Fresnel propagator:
# 1. If $\Delta x > \lambda z / L$, use Transfer Function. 
# 2. If $\Delta x < \lambda z / L$, use Impulse Response.

# In[1]:

import numpy as np
from pyfftw.interfaces.numpy_fft import fftshift, ifftshift, fftfreq, fft2, ifft2


# In[2]:

def propagate(beam_profile : np.ndarray, 
              step_size : float, 
              wavelength : float, 
              prop_dist : float, 
              feature_width : float = 0):
    """If feature_width is 0, then it assumes that the support side length is the feature width."""
    
    w = feature_width if feature_width > 0 else support_side_length
    fresnel_number = w**2 / (wavelength * prop_dist)
    support_side_length = np.max(beam_profile.shape)
    
    if fresnel_number < 0.1:
        print("Using Fraunhofer diffraction.")
        return propFF(beam_profile, step_size, support_side_length, wavelength, prop_dist)
    
    if step_size > (wavelength * prop_dist):
        print("Using Fresnel Transfer Function")
        return propTF(beam_profile, step_size, support_side_length, wavelength, prop_dist)
    
    print("Using Fresnel Impulse Response.")
    return propIR(beam_profile, step_size, support_side_length, wavelength, prop_dist)
    


# In[4]:

def propIR(beam_profile : np.ndarray, 
           step_size : float, 
           support_side_length : float, 
           wavelength : float, 
           prop_dist : float):
    '''
    Propogation using the Impulse Response function. 
    The convention of shiftinng a function in realspace before performing 
    the fourier transform which is used in the reference is followed here.
    '''
    M,N = np.shape(beam_profile)
    k = 2 * np.pi / wavelength
    x = np.linspace(-support_side_length / 2.0, support_side_length / 2.0 - step_size, M)
    y = np.linspace(-support_side_length/  2.0, support_side_length / 2.0 - step_size, N)
    X,Y = np.meshgrid(x, y)
    h = (np.exp(1j * k * prop_dist) / (1j * wavelength * prop_dist) * 
         np.exp(1j * k * (1 / (2 * prop_dist)) * (X**2 + Y**2)))
    
    H = fftshift(fft2(fftshift(h))) * step_size * step_size
    beam_profile_ft = fftshift(fft2(fftshift(beam_profile)))
    out_beam = ifftshift(ifft2(ifftshift(H * beam_profile_ft)))
    return out_beam


# In[5]:

def propFF(beam_profile : np.ndarray, 
              step_size : float, 
              support_side_length : float, 
              wavelength : float, 
              prop_dist : float):
    '''
    Fraunhofer propogation. 
    Note that we now output two variables since the side length 
    of the observation plane is no longer the same as the side length of the input plane.
    '''
    M,N = np.shape(beam_profile)
    k = 2 * np.pi / wavelength
    support_side_length_2 = wavelength * z / step_size
    step_size_2 = wavelength * z / support_side_length
    n = int(support_side_length_2 / step_size_2) #number of samples
    x2 = np.linspace(-support_side_length_2 / 2.0, support_side_length_2 /2.0 , n)
    X2,Y2 = np.meshgrid(x2, x2)
    c = 1 / (1j * wavelength * prop_dist) * np.exp(((1j * k) / (2. * prop_dist)) * (X2**2 + Y2**2))
    out_beam = np.multiply(c, ifftshift(fft2(fftshift(beam_profile)))) * step_size * step_size
    return out_beam, support_side_length_2


# In[ ]:

def propTF(beam_profile : np.ndarray, 
           step_size : float, 
           support_side_length : float, 
           wavelength : float, 
           prop_dist : float):
    '''Propogation using the Transfer function method. 
    '''
    
    M,N = np.shape(beam_profile)
    k = 2 * np.pi / wavelength
    
    fx = fftfreq(M,d=step_size)
    fy = fftfreq(N,d=step_size)
    
    FX,FY = np.meshgrid((fx),(fy))
    FX = fftshift(FX)
    FY = fftshift(FY)
    
    H = np.exp(-1j * np.pi * wavelength * prop_dist * (FX**2 + FY**2))
    H = fftshift(H)
    U1 = fft2(fftshift(beam_profile))
    U2 = H * U1
    out_beam = ifftshift(ifft2(U2))
    return out_beam

