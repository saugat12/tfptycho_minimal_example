
# coding: utf-8

# # Using Fraunhofer propagation to calculate the diffraction intensities

# In[2]:

import numpy as np
import scipy
import Probe
from utilities import getTestImage
from pyfftw.interfaces.numpy_fft import fftshift, ifftshift, fft2, ifft2


# In[34]:

class Detector:
    def __init__(self, 
                 wavelength,
                 det_zdist, 
                 det_pix_pitch,  
                 det_pix_num):
        
        self.zdist = det_zdist
        self.pix_pitch = det_pix_pitch
        self.npix = det_pix_num
        self.width = self.npix * self.pix_pitch
    


# In[35]:

class ObjectData:
    def __init__(self, wavelength, det):

        self.pix_pitch = wavelength * det.zdist / det.width
        # Calculating the Nyquist limit for the sampling
        self.nyquist_apert = wavelength * det.zdist / det.pix_pitch 
        # Since phase information is lost, we have to oversample by a factor of 2.
        self.max_apert = self.nyquist_apert / 2 
        self.vals = None
        
    def populate(self, data):
        self.vals = data.astype('complex64')
        
    def padData(self, num_steps, step_pix, det_npix):
        pad = num_steps  * step_pix + det_npix - self.vals.shape[0]
        if pad != 0:
            print("Adding padding to the object so that each probe step covers equal area.")
            self.vals = np.pad(self.vals, pad_width=((0, pad),(0, pad)), 
                                   mode='constant', constant_values=0)
        
        


# In[1]:

class PtychographySimulation:
    """Creates an array of diffraction data for a ptychographic simulation in the Fraunhofer propagation condition.
    
        Assumptions/Notes:
            1) Square detector with the same pixel pitch in both dimensions.
            2) Steps in probe positions are rounded to integer pixels. 
            (I do not know how to account for sub-pixel movement.)
            3) Probe is moved in a cartesian grid, with equal spacing between the vertical and horizontal grid points. 
        
        """
    
    def __init__(self, image_data, 
                 wavelength : float = 0.142e-9,
                 det_zdist : float = 14.6, 
                 det_pix_pitch : float = 55e-6,  
                 det_pix_num : int = 256,
                 probe_width_dist : float = 3.769e-5,
                 probe_step_dist : float = 4.711e-6,
                 probe_type : str = 'square',
                 probe_n_photons : float = 1e9,
                 probe_propagation_dist : float = 0,
                 noise : bool = True,
                 nyquist_sampling_check : bool = True):
        """
        Parameters:
        (Default parameters for the wavelength, zdist, det_pix_pitch, and det_pix_num correspond to parameters in
        https://journals.aps.org/pra/abstract/10.1103/PhysRevA.87.053850)
        
        wavelength: 
        Wavelength of incident xray in meters. Default value is 8.7 keV
        
        det_zdist:
        Distance of detector from object. Default value is 14.6 m.
        
        det_pix_pitch: 
        Pitch of each pixel in the detector. 
        
        det_pix_num:
        The detector has det_pix_num x det_pix_num pixels.
        
        probe_width_dist: 
        Width of the probe beam incident on the object. Assumes a square or a circularly symmetric probe.
        Used as the diameter for circularly symmetric probe types.
        
        probe_step_dist:
        Distance by which the probe is moved for each step. Assumes a square cartesian grid.
        
        probe_type:
        Type of probe beam to use in the simulation. Options are "square", "circ", and "airy".
        
        probe_n_photons:
        Number of photons in the probe beam. Default is 1bn.
        
        noise:
        Adds poisson noise to the diffraction patterns.
        """
        self.probe_types = {'square':Probe.SquareProbe,
                            'circ': Probe.CircProbe,
                            'complex_circ' : Probe.ComplexCircProbe,
                            'airy': Probe.AiryProbe}
        
        self.wavelength = wavelength
        self.noise = noise
        self.nyquist_sampling_check = nyquist_sampling_check
        
        # Initializing the detector and the object
        self.det = Detector(wavelength, det_zdist, det_pix_pitch, det_pix_num)
        self.obj = ObjectData(wavelength, self.det)
        
        # Initializing the probe
        assert probe_type in self.probe_types.keys(), "Invalid probe type."

        self.probe = self.probe_types[probe_type](wavelength, 
                                                  probe_width_dist, 
                                                  self.obj.pix_pitch,
                                                  probe_n_photons,
                                                  probe_propagation_dist)
        
        self.step_dist = probe_step_dist
        self.step_pix = np.ceil(self.step_dist / self.obj.pix_pitch).astype('int')
        
        self.sanityCheck()      
        self.obj.populate(image_data)
        
        self.num_steps = np.ceil((self.obj.vals.shape[0] - self.det.npix)                                   / self.step_pix).astype('int')
        self.num_diff_patterns = self.num_steps + 1

        self.diffraction_intensities = np.zeros((self.num_diff_patterns, self.num_diff_patterns, 
                                                 self.det.npix, self.det.npix), 
                                                 dtype='float32')
        
        self.obj.padData(self.num_steps, self.step_pix, self.det.npix)
        
        #self.probe.setData(*self.obj.vals.shape)
        self.probe.setData(self.det.npix, self.det.npix)
    
    def sanityCheck(self):
        
        # To ensure overlapping diffraction patterns
        error_str1 = "Neighboring diffraction patterns do not overlap"
        #print(self.probe.npix, self.step_pix)
        assert(self.probe.npix > self.step_pix), error_str1
        
        if not self.nyquist_sampling_check: return
        # To ensure there is enough sampling in each diffraction pattern
        error_str2 = "Current probe width is {}, ".format(self.probe.width_dist) +                      "and the maximum applicable object/illumination size is {}. ".format(self.obj.max_apert) +                      "Not enough sampling in each diffraction pattern. " +                      "Finer sampling required, either in recipropal space (smaller pixels), " +                      "or in real space (smaller probe size)."
        assert(self.probe.width_dist < self.obj.max_apert), error_str2
    
    def calculateDiffraction(self):
 
        for r in range(self.num_diff_patterns):
            for c in range(self.num_diff_patterns):
                obj_view = np.roll(self.obj.vals, 
                                   (-r * self.step_pix, -c * self.step_pix), 
                                   (0,1))[:self.det.npix, :self.det.npix]
                out_wave = self.probe.vals * obj_view
                diff = np.abs(fftshift(fft2(fftshift(out_wave))))**2
                if self.noise:
                    diff = np.random.poisson(diff)
                self.diffraction_intensities[r,c] = diff

