
# coding: utf-8

# ## Theory for the airy disk
# ### - The Airy disk is the Fraunhofer diffraction pattern for illimunation through a circular aperture
# 
# For diffraction at regime 
# $$ z >> \left(\frac{k(\xi^2 + \eta^2)}{2}\right)_{\mathrm{max}},$$
# where the aperture is at $z=0$, we can approximate the wavefield as
# $$ U_2(x_2,y_2,z) = \frac{e^{j k z}e^{j \frac{k}{2z} (x^2 + y^2)}}{j \lambda z} 
#                     \iint\limits_{-\infty}^{\infty} U_1(\xi, \eta, 0)
#                     \exp\left[-j\frac{2\pi}{\lambda z}(x\xi + y\eta)\right]d\xi d\eta.$$
#                     
# For a circular aperture, the field immediately beyond the aperture is
# $$U_1(\xi, \eta) = \mathrm{circ}\left(\frac{\sqrt{\xi^2 + \eta^2}}{w}\right).$$
# 
# To calculate the Fraunhofer diffraction, we need the Fourier transform
# $$\mathcal{F}\{U_1(\xi,\eta)\} = w^2 \frac{J_1\left(2\pi w \sqrt{f_\xi^2 + f_\eta^2}\right)}
#                                           {w\sqrt{f_\xi^2 + f_\eta^2}},$$
# where $J_1$ is a Bessel function, $f_\xi = x/\lambda z$, and $f_\eta = y/\lambda z$. 
# Then, the wavefield is calculated to be:
# \begin{align}
# U_2(x,y) = &\frac{\exp(j k z)}{j\lambda z} \exp\left(j \frac{k}{2z} \left(x^2 + y^2\right)\right)\\
#            & \times w^2 J_1\left( 2\pi \frac{w}{\lambda z}\sqrt{x^2 + y^2}\right) \\
#            & \times \frac{1}{\frac{w}{\lambda z}\sqrt{x^2 + y^2}}.
# \end{align}   

# ### Probe parameters
# 
# We have shown that the probe wavefield can be written as
# \begin{align}
# P(\mathbf{r}) = P(x,y) = &\frac{\exp(j k z)}{j\lambda z} \exp\left(j \frac{k}{2z} \left(x^2 + y^2\right)\right)\\
#                          & \times w^2 J_1\left( 2\pi \frac{w}{\lambda z}\sqrt{x^2 + y^2}\right) \\
#                          & \times \frac{1}{\frac{w}{\lambda z}\sqrt{x^2 + y^2}}.
# \end{align}  
# 
# We can choose the values of $w$, $\lambda$, and $z$ to get the desired probe wavefield.
# 
# Note that, for Fraunhofer diffraction, the Fresnel number constraint requires that $N_F = w^2 / \lambda z << 1$. Given values of $w$ and $\lambda$, we have to choose $z$ such that $N_F < 0.1$.
# 
# ### Estimating the aperture radius $w$ given a probe radius $R$ 
# 
# When we require a probe beam that has a radius $R$ at the sample, then we need to set $w$ such that the radius of the zeroth order maximum of the airy pattern is $R$. This happens when we hit the first zero of the Bessel function. Mathematically, 
# $$ J_1(\gamma) = 0$$
# when 
# $$\gamma = 3.832.$$
# Writing 
# $$\sqrt{x^2 + y^2} = R,$$
# we get
# $$\frac{w}{z} = 3.832\frac{\lambda}{2\pi R}.$$
# 
# Summarizing, we have to set $w$ and $z$ such that
# $$\frac{w^2}{\lambda z} = 0.1,$$
# and,
# $$\frac{w}{\lambda z} = \frac{3.832}{2\pi R}.$$

# Finally, this gives us,
# \begin{align}
# w &= 0.1\frac{2\pi R}{3.832}\\
# z &= 0.1\frac{(2 \pi R)^2}{(3.832)^2 \lambda}
# \end{align}

# ## Other probes (Obvious):
# 1. Square aperture
# 2. Circular aperture

# In[37]:

import numpy as np
from scipy import special
import Propagators
from functools import wraps


# In[38]:

class Probe(object):
    def __init__(self, wavelength,
                 width_dist,
                 pixel_pitch,
                 n_photons = 1e9,
                 propagation_dist = 0):
        self.width_dist = width_dist
        self.wavelength = wavelength
        self.center_dist = np.array([0,0])
        self.pix_pitch = pixel_pitch
        self.n_photons = n_photons
        self.propagation_dist = propagation_dist
         
        self.npix = np.ceil(width_dist / pixel_pitch).astype('int')
        self.center_dist = np.array([self.width_dist / 2, self.width_dist / 2])
        self.xx = None
        self.yy = None
        self.view_width_dist = None
        self.vals = None
        
    def setData(self, n_rows, n_cols):
        xdists = np.arange(n_cols, dtype='float32') * self.pix_pitch
        ydists = np.arange(n_rows, dtype='float32') * self.pix_pitch
        self.xx, self.yy = np.meshgrid(xdists, ydists)
        self.view_width_dist = (n_rows + 1) * self.pix_pitch
        self.center_dist = np.array([self.view_width_dist / 2, self.view_width_dist / 2])
        return
    
    def updateWidth(self):
        intensities = np.abs(self.vals)**2
        total_power = intensities.sum()
        
        minx1 = np.ceil( (self.center_dist[0] - self.width_dist / 2) / self.pix_pitch).astype('int')
        minx2 = np.ceil( (self.center_dist[0] + self.width_dist / 2) / self.pix_pitch).astype('int')

        i = 0
        while True:
            id1 = minx1 - i
            id2 = minx2 + i
            
            contained_power = intensities[id1:id2, id1:id2].sum()
            percent = contained_power / total_power * 100
            if percent > 99.99: 
                break
            i += 1

        self.npix += 2 * i
        if self.npix > self.vals.shape[0] : self.npix = self.vals.shape[0]
        self.width_dist = self.npix * self.pix_pitch
    
    def propagate(func):
        @wraps(func)
        def wrapper(self, *args, **kwargs):
            func(self, *args, **kwargs)
            if self.propagation_dist > 0: 
                self.vals = Propagators.propagate(self.vals,
                                                  self.pix_pitch,
                                                  self.wavelength, 
                                                  self.propagation_dist,
                                                  self.width_dist)
            self.updateWidth()
            return
        return wrapper
    


# In[39]:

class SquareProbe(Probe): 
    def setData(self, *args):
        super().setData(*args)
        self.calculateBeam()
    
    @Probe.propagate
    def calculateBeam(self):
        t1 = np.where( np.abs((self.xx - self.center_dist[0])) <= self.width_dist / 2, 1, 0)
        t2 = np.where( np.abs((self.yy - self.center_dist[1])) <= self.width_dist / 2, 1, 0)
        self.vals = (t1 * t2).astype('complex64')
        self.vals *= np.sqrt(self.n_photons / np.sum(self.vals * self.vals.conj()))


# In[40]:

class CircProbe(Probe):
    def setData(self, *args):
        super().setData(*args)
        self.calculateBeam()
    
    @Probe.propagate
    def calculateBeam(self):
        radius = self.width_dist / 2
        radsq = (self.xx - self.center_dist[0])**2 + (self.yy - self.center_dist[0])**2
        self.vals = np.where( radsq < radius**2, 1, 0).astype('complex64')
        self.vals *= np.sqrt(self.n_photons / np.sum(self.vals * self.vals.conj()))


# In[1]:

class ComplexCircProbe(Probe):
    def setData(self, *args):
        super().setData(*args)
        self.calculateBeam()
    
    @Probe.propagate
    def calculateBeam(self):
        radius = self.width_dist / 2
        radsq = (self.xx - self.center_dist[0])**2 + (self.yy - self.center_dist[0])**2
        self.vals = np.where( radsq < radius**2, 1, 0).astype('complex64')
        self.vals *= np.exp(- 1.5 * np.sqrt(radsq) / radius)
        
        phases = np.where(radsq < radius**2, 1, 0) * np.sqrt(radsq) / radius * np.pi 
        self.vals *= np.exp(1j * phases)
        self.vals *= np.sqrt(self.n_photons / np.sum(self.vals * self.vals.conj()))
        


# In[42]:

class AiryProbe(Probe):
    def setData(self, *args):
        super().setData(*args)
        self.calculateBeam()
    
    def jinc(self,x):
        """The jinc function.

        J1(2*pi*x)/x

        Note that as x->0, J1(2*pi*x)/x -> pi.

        Since we want to avoid dividing by zero, we use a mask."""

        mask = (x != 0)

        # Setting default output value that accounts for x=0
        out = np.pi * np.ones(x.shape)
        out[mask] = special.j1(x[mask] * 2 * np.pi) / x[mask]
        return out
    
    def apertureWidth(self, radius):
        """ Calculates the aperture width using the beam radius at the sample.
        
        Uses the Fresnel number condition for Fraunhofer propagation,  
        and the first zero of the J1 bessel function.
        """
        w = 0.1 * 2 * np.pi * radius / (special.jn_zeros(1,1))
        return w
    
    def propagationDist(self, radius, wavelength):
        """ Calculate the propagation distance from the aperture to the sample.
        
        Uses the Fresnel number condition for Fraunhofer propagation, and the
        first zero of the J1 bessel function.
        """
        z = 0.1 * (2 * np.pi * radius)**2 / (special.jn_zeros(1,1)**2 * wavelength)
        return z
    
    @Probe.propagate
    def calculateBeam(self):
        "Calculating beam"
        radius = self.width_dist / 2
        w = self.apertureWidth(radius)
        z = self.propagationDist(radius, self.wavelength)
        k = 2 * np.pi / self.wavelength
        lz = self.wavelength * z
        S = (self.xx - self.center_dist[0])**2 + (self.yy - self.center_dist[1])**2
        
        # wavefield 
        term1 = np.exp(1j * k * z) / (1j * lz)
        term2 = np.exp(1j * k * S / (2 * z))
        term3 = w**2 * self.jinc( np.sqrt(S) * w / lz)
        self.vals = (term1 * term2 * term3).astype('complex64')
        self.vals *= np.sqrt(self.n_photons / np.sum(self.vals * self.vals.conj()))


# In[ ]:



