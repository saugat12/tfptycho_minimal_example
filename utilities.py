from PIL import Image
import numpy as np
import scipy.fftpack as ft

def getTestImage(mod='modulus.png', phase='phase.png'):
    modulus = np.asarray(Image.open(mod), dtype='float')[::-1]
    phase = np.asarray(Image.open(phase), dtype='float')[::-1]
    modulus = modulus / modulus.max()
    phase = phase / phase.max() * np.pi - np.pi / 2
    image = modulus * np.exp(1j * phase)
    return image

def calcError(sim_obj, phase_ret_obj):
    ref = sim_obj.obj.vals
    test = phase_ret_obj.obj
    indx1 = ref.shape[0] // 2 - 64
    indx2 = ref.shape[0] // 2 + 64
    err = np.mean(np.abs(ref[indx1:indx2, indx1:indx2] - test[indx1:indx2, indx1:indx2]))
    return err

def getSumOperator(N_pixels, n_synthetic_subpixels):
    arr1 = np.zeros(N_pixels * N_pixels, dtype='float32')
    n_synthetic_subpixels = n_synthetic_subpixels

    n_blocks = N_pixels //  n_synthetic_subpixels

    for i in range( n_synthetic_subpixels):
        arr1[i * N_pixels : i * N_pixels +  n_synthetic_subpixels] = 1

    M = np.zeros((N_pixels * N_pixels //  n_synthetic_subpixels**2, N_pixels * N_pixels), dtype='float32')

    for i in range( M.shape[0] ):
        arr = np.roll(arr1, (i *  n_synthetic_subpixels) +  ( n_synthetic_subpixels - 1) * N_pixels  * (i // n_blocks) )
        M[i] = arr

    return M

def getIdctOperator(N):
    c1 = ft.idct(np.eye(N, dtype='float32'), norm='ortho', axis=0)
    C = np.kron(c1, c1)
    return C 

def getDctOperator(N):
    c1 = ft.dct(np.eye(N, dtype='float32'), norm='ortho', axis=0)
    C = np.kron(c1, c1)
    return C 

def binAllDiffractionPatterns(diffraction_intensities, n_synthetic_subpixels):
    diff_all = np.zeros(diffraction_intensities.shape, dtype='float32')
    
    for id1, dim1 in tqdm(enumerate(diffraction_intensities)):
        for id2, dim2 in enumerate(dim1): 
            diff[id1, id2] = binSingleDiffractionPattern(dim2, n_synthetic_subpixels)
            
    return diff

def binSingleDiffractionPattern(diffraction_pattern, n_synthetic_subpixels):

    n_blocks = np.ceil(diffraction_pattern.shape[0] / n_synthetic_subpixels).astype('int')
    required_padding = n_blocks * n_synthetic_subpixels - diffraction_pattern.shape[0]
    temp = np.pad(diffraction_pattern, pad_width=(0, required_padding), mode='edge')
    t1 = temp.reshape(-1, n_blocks, n_synthetic_subpixels).sum(axis=-1)
    t2 = t1.T.reshape(-1, n_blocks, n_synthetic_subpixels).sum(axis=-1).T
    t3 = t2

    t4 = np.zeros(temp.shape)
    t5 = t4.reshape(n_synthetic_subpixels,n_synthetic_subpixels, n_blocks,n_blocks) 
    t6 = t5 + t3
    t7 = t6.transpose(2,1,3,0).reshape(temp.shape)  
    diff = t7[:diffraction_pattern.shape[0], :diffraction_pattern.shape[1]]

    return diff

def rescale(img, downsampling_factor):
    abs_img = np.abs(img)
    ang_img = np.angle(img)
    n_blocks = abs_img.shape[0] // downsampling_factor
    rescaled = []
    for im in [abs_img, ang_img]:
        t1 = im.reshape(-1, n_blocks, downsampling_factor).sum(axis=-1)
        t2 = t1.T.reshape(-1, n_blocks, downsampling_factor).sum(axis=-1).T
        t3 = t2 / downsampling_factor**2
        rescaled.append( t3)
    return rescaled[0] * np.exp(rescaled[1] * 1j)